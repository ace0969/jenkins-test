variable "RDS_PASSWORD" {
  default = "somepassword"
}

variable "API_PORT" {
  default = "5432"
}

variable "WEB_PORT" {
  default =  "8080"
}

variable "AWS_REGION" {
  default = "ap-northeast-2"
}

variable "PATH_TO_PRIVATE_KEY" {
  default = "key-jenkins"
}
variable "PATH_TO_PUBLIC_KEY" {
  default = "key-jenkins.pub"
}


variable "AMIS" {
  type = "map"
  default = {
    ap-northeast-2 = "ami-0a25005e83c56767a"
    us-west-2 = "ami-0653e888ec96eab9b"
    eu-west-1 = "ami-09f0b8b3e41191524"
  }
}

variable "INSTANCE_USERNAME" {
  default = "ubuntu"
}
