resource "aws_key_pair" "key-jenkins" {
  key_name = "key-jenkins"
  public_key = "${file("${var.PATH_TO_PUBLIC_KEY}")}"
  lifecycle {
    ignore_changes = ["public_key"]
  }
}
